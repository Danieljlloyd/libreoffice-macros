"""
A LibreOffice Macro to sort a Gantt Chart.
"""

import json
from com.sun.star.table.CellContentType import EMPTY, VALUE, TEXT, FORMULA

def get_cell_data(cell):
    """
    Function to read data from a cell into the correct format based on the
    cell content type.
    """

    result = None

    if cell.Type == VALUE:
        result = cell.getValue()
    elif cell.Type == TEXT:
        result = cell.getString()
    elif cell.Type == FORMULA:
        result = cell.getFormula()

    return result

def read_table(sheet, max_items=250000, max_fields=100):
    """
    Function to slurp a normally formatted sheet into a list of dictionaries
    format to be easily used for more intense processing.
    """

    # Get the name of the fields
    fields = []
    for i in range(max_fields):
        cell = sheet.getCellByPosition(i, 0)
        cell_data = get_cell_data(cell)

        if cell_data is None:
            break
        else:
            fields.append(cell_data)

    # Create the list of dictionaries structure.
    objects = []
    for row in range(max_items):
        new_object = {}

        cell = sheet.getCellByPosition(1, row+1)
        cell_data = get_cell_data(cell)
        if cell_data is None:
            break

        for i in range(len(fields)):
            cell = sheet.getCellByPosition(i, row+1)
            cell_data = get_cell_data(cell)
            new_object[ fields[i] ] = cell_data

        objects.append(new_object)

    return objects

def test_read_table(*args):
    desktop = XSCRIPTCONTEXT.getDesktop()
    model = desktop.getCurrentComponent()
    sheet = model.Sheets.getByName('Sheet1')

    objects = read_table(sheet)

    cell = sheet.getCellByPosition(10,10)

    cell.setString(json.dumps(objects))
