"""
A LibreOffice Macro to sort a Gantt Chart.
"""

import json
from com.sun.star.table.CellContentType import EMPTY, VALUE, TEXT, FORMULA

from datetime import datetime, timedelta

LO_BASE_DATE = datetime(year=1899, month=12, day=30)

def datetime_to_lodate(my_datetime):
    return (my_datetime - LO_BASE_DATE).days

def lodate_to_datetime(lodate):
    return (LO_BASE_DATE + timedelta(days=lodate))

def add_business_days(start_date, business_days):
    """
    Go forward n business days from a start date.
    """

    # Calculate the component due to full weeks.
    whole_week_weekends = (business_days / 5) * 2

    # Calculate the component from crossing the weekend.
    if (business_days % 5) + start_date.weekday() > 4:
        partial_week_weekends = 2
    else:
        partial_week_weekends = 0

    total_days = business_days + whole_week_weekends + partial_week_weekends

    end_date = start_date + timedelta(days=total_days)

    return end_date


def get_cell_data(cell):
    """
    Function to read data from a cell into the correct format based on the
    cell content type.
    """

    result = None

    if cell.Type == VALUE:
        result = cell.getValue()
    elif cell.Type == TEXT:
        result = cell.getString()
    elif cell.Type == FORMULA:
        result = cell.getFormula()

    return result

def get_fields(sheet, max_fields=100):
    """
    Gets a tables fields from the first row in a sheet.
    """

    fields = []
    for i in range(max_fields):
        cell = sheet.getCellByPosition(i, 0)
        cell_data = get_cell_data(cell)

        if cell_data is None:
            break
        else:
            fields.append(cell_data)

    return fields


def read_table(sheet, max_items=250000):
    """
    Function to slurp a normally formatted sheet into a list of dictionaries
    format to be easily used for more intense processing.
    """

    fields = get_fields(sheet)

    objects = []
    for row in range(max_items):
        new_object = {}

        cell = sheet.getCellByPosition(1, row+1)
        cell_data = get_cell_data(cell)
        if cell_data is None:
            break

        for i in range(len(fields)):
            cell = sheet.getCellByPosition(i, row+1)
            cell_data = get_cell_data(cell)
            new_object[ fields[i] ] = cell_data

        objects.append(new_object)

    return objects

def dump_table(sheet, objects):
    """
    Function to dump a list of dictionaries into a table that already
    has headers.
    """

    fields = get_fields(sheet)

    for i in range(len(objects)):
        for j in range(len(fields)):
            cell = sheet.getCellByPosition(j, i+1)
            data = objects[i][ fields[j] ]

            if type(data) is float or type(data) is int:
                cell.setValue(data)

            elif type(data) is str:
                if data[0] == '=':
                    cell.setFormula(data)
                else:
                    cell.setString(data)

            elif data is None:
                cell.setString('')

def schedule_tasks(tasks):
    for task in tasks:
        start_date = lodate_to_datetime(task['Date Available'])
        depstr = task['Dependencies']

        # Find the correct starting date given the dependencies.
        if depstr:
            deps = [int(t) for t in depstr.split(',')]

            for dep in deps:
                for deptask in tasks:
                    dep_end_date = lodate_to_datetime(deptask['End Date'])

                    if deptask['ID'] == dep and dep_end_date > start_date:
                        start_date = dep_end_date

        # Calculate the end date from the dependencies.
        duration = task['Duration (Days)']
        end_date = add_business_days(start_date, duration)

        # Update the task.
        task['Start Date'] = datetime_to_lodate(start_date)
        task['End Date'] = datetime_to_lodate(end_date)

    return tasks

def update_gantt(*args):
    desktop = XSCRIPTCONTEXT.getDesktop()
    model = desktop.getCurrentComponent()
    sheet = model.Sheets.getByName('Schedule')

    tasks = read_table(sheet)

    tasks = schedule_tasks(tasks)

    cell = sheet.getCellByPosition(10,10)

    dump_table(sheet, tasks)
