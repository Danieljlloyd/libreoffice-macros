import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name='libreoffice_macros',
    version='0.0.1',
    author='Daniel Lloyd',
    author_email='DanielLloyd7@gmail.com',
    description='A repository for my Python LibreOffice macros.',
    license='GPL',
    keywords='libreoffice openoffice macro python',
    url='',
    packages=['util'],
    scripts=[],
    long_description=read('README.md'),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Topic :: Utilities',
        'License :: OSI Approved :: GPL License'
    ]
)
